import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import styles from './menu.scss';

const MenuItem = (props) => {
  const { item } = props;
  return (
    <li className={`${styles.navigation__item}`} key={item.title}>
      <NavLink
        exact
        to={item.link}
        className={`${styles.navigation__link}`}
        activeClassName={`${styles.navigation__link_active}`}
      >
        {item.icon}
        <span>{item.title}</span>
      </NavLink>
    </li>
  );
};

MenuItem.propTypes = {
  item: PropTypes.instanceOf(Object).isRequired,
};

export default MenuItem;
