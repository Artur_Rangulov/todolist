import React from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import MenuItem from './MenuItem';
import styles from './menu.scss';

const Menu = (props) => {
  const { links } = props;
  return (
    <div className={`${styles.navigation}`}>
      <ul className={`${styles.navigation__items}`}>
        {links.map(item => <MenuItem key={uuid()} item={item} />)}
      </ul>
    </div>
  );
};

Menu.propTypes = {
  links: PropTypes.instanceOf(Array).isRequired,
};

export default Menu;
