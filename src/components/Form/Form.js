import React from 'react'

export const Form = props => {
    const {onSubmit, children} = props;
    return (
        <form onSubmit={onSubmit}>
            {children}
        </form>
    )
}

