/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import styles from './input.scss';

const Input = ({
  value,
  onChange,
  name,
  placeholder,
  label,
  description,
  require,
}) => (
  <div className="form-group">
    <label htmlFor={name}>
      {label}
      {require ? <span className={styles.require}>*</span> : null}
      <small className="form-text text-muted">
        {description}
      </small>
    </label>
    <input
      type="text"
      className="form-control"
      id={name}
      name={name}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  </div>
);

Input.propTypes = {
  value: PropTypes.string,
  description: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
  require: PropTypes.bool,
};

Input.defaultProps = {
  value: '',
  description: '',
  label: '',
  placeholder: '',
  name: '',
  onChange: () => {},
  require: false,
};

export default Input;
