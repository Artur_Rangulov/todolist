import React from 'react';
import PropTypes from 'prop-types';
import './button.scss';

const Button = (props) => {
  const {
    value,
    onClick,
    disabled,
    primary,
    danger,
  } = props;

  const classes = ['btn'];

  if (primary) {
    classes.push('btn-primary');
  }

  if (danger) {
    classes.push('btn-danger');
  }
  return (
    <button
      className={classes.join(' ')}
      type="button"
      onClick={onClick}
      disabled={disabled}
    >
      {value}
    </button>
  );
};

Button.propTypes = {
  value: PropTypes.string,
  primary: PropTypes.bool,
  danger: PropTypes.bool,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  value: '',
  primary: true,
  danger: false,
  onClick: () => {},
  disabled: false,
};

export default Button;
