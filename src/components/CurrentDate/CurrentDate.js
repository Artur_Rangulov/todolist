import React, { Component } from 'react';

export default class CurrentDate extends Component {
  state = { time: new Date().toLocaleTimeString() };

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      time: new Date().toLocaleTimeString(),
    });
  }

  render() {
    const { time } = this.state;
    return (
      <>
        {time}
      </>
    );
  }
}
