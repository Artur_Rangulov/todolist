import React from 'react'
import PropTypes from 'prop-types';

const Select = props => {
    const {options, onChange} = props
    return (
        <select className="custom-select" onChange={onChange}>
            {options
                ? options.map((option, index) => {
                   return <option key={index} value={option.value} selected={option.selected}>{option.label}</option>
                    })
                : null
            }
        </select>
    )
}

Select.propTypes = {
    options: PropTypes.array,
    
}

export default Select
