import React from 'react';
import PropTypes from 'prop-types';
import Badge from '../Badge/Badge';
import {
  Delete,
  ArrowRight,
  ArrowLeft,
  Complete,
} from '../Glyph/Glyph';

const Card = ({
  tasks,
  deleteItem,
  sendToListOfTasks,
  sendToMatrix,
  completeTask,
  cardtype,
}) => (
  <>
    {tasks !== [] && !!tasks && tasks.map(task => (
      <div className="card mb-3" key={task.id}>
        <div className="card-body d-flex">
          <div className="">
            <h5 className="card-title">
              {task.taskName}
              &nbsp;
              <Badge value={task.taskImportanceNumber} />
            </h5>
            <p className="card-text">
              {task.taskDescription}
              id:
              {task.id}
            </p>
          </div>
          <div className="d-flex flex-column justify-content-between">
            {cardtype === 'todolist' ? <ButtonsToDoList completeTask={completeTask} sendToMatrix={sendToMatrix} id={task.id} /> : null}
            {cardtype === 'taskslist' ? <ButtonsMatrix sendToListOfTasks={sendToListOfTasks} deleteItem={deleteItem} id={task.id} /> : null}
          </div>
        </div>
      </div>
    ))}
  </>
);

export const ButtonsMatrix = ({ sendToListOfTasks, deleteItem, id }) => (
  <>
    <button type="button" title="Send to trash" onClick={() => deleteItem(id)}>
      <Delete color="#ff6666" />
    </button>
    <button type="button" title="Send to tasks" onClick={() => sendToListOfTasks(id)}>
      <ArrowRight color="#007bff" />
    </button>
  </>
);

const ButtonsToDoList = (props) => {
  const { completeTask, sendToMatrix, id } = props;
  return (
    <>
      <button type="button" title="Complete task" onClick={() => completeTask(id)}>
        <Complete color="#32CD32" />
      </button>
      <button type="button" title="Send to Matrix" onClick={() => sendToMatrix(id)}>
        <ArrowLeft color="#007bff" />
      </button>
    </>
  );
};

ButtonsToDoList.propTypes = {
  completeTask: PropTypes.func.isRequired,
  sendToMatrix: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
};

Card.propTypes = {
  tasks: PropTypes.instanceOf(Array),
  deleteItem: PropTypes.func,
  sendToListOfTasks: PropTypes.func,
  sendToMatrix: PropTypes.func,
  completeTask: PropTypes.func,
  cardtype: PropTypes.string,
};

Card.defaultProps = {
  tasks: [],
  cardtype: '',
  deleteItem: () => {},
  sendToListOfTasks: () => {},
  sendToMatrix: () => {},
  completeTask: () => {},
};

ButtonsMatrix.propTypes = {
  sendToListOfTasks: PropTypes.func.isRequired,
  deleteItem: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
};

export default Card;
