import React from 'react';
import PropTypes from 'prop-types';
import styles from './Toggle.scss';

const Toggle = ({
  checked,
  onChange,
  valueFalse,
  valueTrue,
}) => (
  <div className={`d-flex align-items-center  ${styles.container}`}>
    <div className={styles.valueLeft}>{valueFalse}</div>
    <div className={styles.appCover}>
      <div className={`${styles.button} ${styles.r} ${styles.button1}`}>
        <input type="checkbox" className={styles.checkbox} onChange={onChange} checked={checked} />
        <div className={styles.knobs} />
        <div className={styles.layer} />
      </div>
    </div>
    <div className={styles.valueRight}>{valueTrue}</div>
  </div>
);

Toggle.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func,
  valueFalse: PropTypes.string,
  valueTrue: PropTypes.string,
};

Toggle.defaultProps = {
  checked: false,
  onChange: () => {},
  valueFalse: '',
  valueTrue: '',
};

export default Toggle;
