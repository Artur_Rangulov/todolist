import React from 'react';
import PropTypes from 'prop-types';

const Badge = (props) => {
  const { value, info } = props;
  let setBadgeColor;

  if (!info) {
    switch (true) {
    case value <= 5:
      setBadgeColor = 'badge-primary'; break;
    case value <= 8:
      setBadgeColor = 'badge-warning'; break;
    default:
      setBadgeColor = 'badge-danger';
    }
  } else {
    setBadgeColor = 'badge-info';
  }

  return <span className={`badge ${setBadgeColor}`}>{value}</span>;
};

Badge.propTypes = {
  info: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};

Badge.defaultProps = {
  info: false,
};

export default Badge;
