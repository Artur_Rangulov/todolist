import axios from 'axios';

export function getNamesSuccess(names) {
  return {
    type: 'GET_NAMES_SUCCESS',
    names,
  };
}

export function getNames() {
  return (dispath) => {
    axios.get('http://localhost:3000/data/names.json')
      .then((response) => {
        if (response.status === 200) {
          dispath(getNamesSuccess(response.data));
          console.log(response.data);
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };
}

export function addName(name) {
  return {
    type: 'ADD_NAME',
    name,
  };
}

export function postNames() {
  return (dispath, getState) => {
    console.log('1', { names: getState().names.names });
    axios.post('http://localhost:3000/data/names.json', { names: getState().names.names })
      .then((response) => {
        console.log(response);
      })
      .catch((error) => {
        console.log(error);
      });
    dispath(getNames());
  };
}
