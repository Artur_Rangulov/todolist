import axios from 'axios';
import {
  SUBMIT_NEW_TASKS,
  DELETE_TASK_ITEM,
  SEND_TO_LIST_OF_TASKS,
  SEND_TO_MATRIX,
  COMPLETE_TASK,
} from './actionsType';

export function submitNewTask(
  taskName,
  taskDescription,
  taskImportanceNumber,
  taskImportance,
  taskUrgency,
) {
  const id = Date.now();
  return {
    type: SUBMIT_NEW_TASKS,
    taskName,
    taskDescription,
    taskImportanceNumber,
    taskImportance,
    taskUrgency,
    id,
  };
}

export function deleteTaskItem(id) {
  return {
    type: DELETE_TASK_ITEM,
    id,
  };
}

export function sendToListOfTasks(id) {
  return {
    type: SEND_TO_LIST_OF_TASKS,
    id,
  };
}

export function sendToMatrix(id) {
  return {
    type: SEND_TO_MATRIX,
    id,
  };
}

export function completeTask(id) {
  return {
    type: COMPLETE_TASK,
    id,
  };
}

export function getTodosSuccess(todos) {
  return {
    type: 'GET_TODOS_SUCCESS',
    todos,
  };
}

export function getTodos() {
  return (dispath) => {
    axios.get('http://localhost:3000/data/todos.json')
      .then((response) => {
        if (response.status === 200) {
          dispath(getTodosSuccess(response.data));
        }
      })
      .catch((error) => {
        console.log('error', error);
      });
  };
}

export function postTodos() {
  return () => {
    console.log('1');
  };
}
