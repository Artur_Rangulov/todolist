import {
  SUBMIT_NEW_TASKS,
  DELETE_TASK_ITEM,
  SEND_TO_LIST_OF_TASKS,
  SEND_TO_MATRIX,
  COMPLETE_TASK,
} from '../actions/actionsType';

const initialState = {};

export default function todoapp(state = initialState, action) {
  const {
    tasks,
    todos,
    completedTasks,
    deletedTodos,
  } = state;

  let searchIndex;

  switch (action.type) {
  case 'GET_TODOS_SUCCESS':
    return {
      ...state,
      ...action.todos,
    };
  case SUBMIT_NEW_TASKS:
    return {
      ...state,
      tasks: [
        ...state.tasks,
        {
          taskName: action.taskName,
          taskImportanceNumber: action.taskImportanceNumber,
          taskDescription: action.taskDescription,
          taskImportance: action.taskImportance,
          taskUrgency: action.taskUrgency,
          id: action.id,
        }],
    };
  case DELETE_TASK_ITEM:
    searchIndex = tasks.findIndex(el => el.id === action.id);
    deletedTodos.push(tasks[searchIndex]);
    return {
      ...state,
      tasks: tasks.filter(el => el.id !== action.id),
      deletedTodos,
    };
  case SEND_TO_LIST_OF_TASKS:
    searchIndex = tasks.findIndex(el => el.id === action.id);
    todos.push(tasks[searchIndex]);
    return {
      ...state,
      todos,
      tasks: tasks.filter(el => el.id !== action.id),
    };
  case SEND_TO_MATRIX:
    searchIndex = todos.findIndex(el => el.id === action.id);
    tasks.push(todos[searchIndex]);
    return {
      ...state,
      todos: todos.filter(el => el.id !== action.id),
      tasks,
    };
  case COMPLETE_TASK:
    searchIndex = todos.findIndex(el => el.id === action.id);
    completedTasks.push(todos[searchIndex]);
    return {
      ...state,
      completedTasks,
      todos: todos.filter(el => el.id !== action.id),
    };
  default:
    return state;
  }
}
