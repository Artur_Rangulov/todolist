import { combineReducers } from 'redux';
import todoapp from './todoapp';
import names from './names';

export default combineReducers({
  todoapp, names,
});
