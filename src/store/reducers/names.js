const initialState = {};

export default function names(state = initialState, action) {
  const list = state.names;
  switch (action.type) {
  case 'GET_NAMES_SUCCESS':
    return {
      ...state,
      ...action.names,
    };
  case 'ADD_NAME':
    list.push({ name: action.name });
    return {
      ...state,
      names: list,
    };
  default:
    return state;
  }
}
