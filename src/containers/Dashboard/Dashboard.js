import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as UI from './DashboardStyle';

const Dashboard = (props) => {
  const {
    completedTasks,
    todos,
    tasks,
    deletedTodos,
  } = props;

  const items = [
    { title: 'Завершенные задачи', count: completedTasks.length },
    { title: 'На сегодня задач', count: todos.length },
    { title: 'Всего задач', count: tasks.length },
    { title: 'Удаленные задачи', count: deletedTodos.length },
  ];

  const cardsItems = items && items.map(item => (
    <UI.Item key={item.title}>
      <UI.ItemTitle>{item.title}</UI.ItemTitle>
      <UI.Count>{item.count}</UI.Count>
    </UI.Item>
  ));

  return (
    <div>
      <UI.Cards>
        {cardsItems}
      </UI.Cards>
    </div>
  );
};

Dashboard.propTypes = {
  completedTasks: PropTypes.instanceOf(Array).isRequired,
  todos: PropTypes.instanceOf(Array).isRequired,
  tasks: PropTypes.instanceOf(Array).isRequired,
  deletedTodos: PropTypes.instanceOf(Array).isRequired,
};

function mapStateToProps(state) {
  return {
    todos: state.todoapp.todos,
    tasks: state.todoapp.tasks,
    completedTasks: state.todoapp.completedTasks,
    deletedTodos: state.todoapp.deletedTodos,
  };
}

export default connect(mapStateToProps)(Dashboard);
