import styled from 'styled-components';

export const Cards = styled.div`
  display: flex;
  flex-wrap: wrap;
  color:#444;
`;

export const Item = styled.div`
  box-sizing:border-box;
  border: 1px solid #ccc;
  border-radius: 5px;
  overflow: hidden;
  padding: 10px 20px;
  margin: 5px;
  flex-basis:24%;
  min-width:170px;
  align-content:center;
`;

export const ItemTitle = styled.div`
  color: #777;
`;

export const Count = styled.div`
  font-size: 2.5em;
  font-weight: bold;
`;
