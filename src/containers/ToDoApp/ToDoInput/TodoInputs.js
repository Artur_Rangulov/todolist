import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Input from '../../../components/Input/Input';
import Button from '../../../components/Button/Button';
import Toggle from '../../../components/Toggle/Toggle';
import './todoinputs.scss';
import { submitNewTask } from '../../../store/actions/todoapp';

class TodoInputs extends Component {
  state = {
    taskName: '',
    taskDescription: '',
    taskImportanceNumber: '',
    taskImportance: false,
    taskUrgency: false,
  };

  onChange = (event) => {
    event.preventDefault();
    const field = event.target.name;
    const { ...state } = this.state;
    this.setState({
      ...state,
      [field]: event.target.value,
    });
  };

  clickHandlerSubmit = () => {
    const {
      taskName,
      taskDescription,
      taskImportanceNumber,
      taskImportance,
      taskUrgency,
    } = this.state;

    const { submitTask } = this.props;

    if (taskName === '' || taskImportanceNumber === '') {
      this.setState({
        error: 'error',
      });
      return;
    }

    submitTask(
      taskName,
      taskDescription,
      taskImportanceNumber,
      taskImportance,
      taskUrgency,
    );

    this.setState({
      taskName: '',
      taskDescription: '',
      taskImportanceNumber: '',
      taskImportance: false,
      taskUrgency: false,
    });
  };

  validate = () => {
    const { taskName, taskImportanceNumber } = this.state;
    if (taskName !== '' && taskImportanceNumber !== '') {
      return false;
    }
    return true;
  };

  changeHandlerImportance = (event) => {
    this.setState({
      taskImportance: event.target.checked,
    });
  };

  changeHandlerUrgency = (event) => {
    this.setState({
      taskUrgency: event.target.checked,
    });
  };

  render() {
    const {
      taskName,
      taskDescription,
      taskImportanceNumber,
      taskImportance,
      taskUrgency,
    } = this.state;
    return (
      <>
        <h4 className="border-bottom pb-3">Добавить новый элемент</h4>
        <div className="todoinputs mt-4">
          <Input
            type="text"
            name="taskName"
            label="Название задачи"
            description=""
            value={taskName}
            onChange={event => this.onChange(event, 'taskName')}
            require
          />
          <Input
            type="text"
            name="taskDescription"
            label="Описание задачи"
            description=""
            value={taskDescription}
            onChange={event => this.onChange(event, 'taskDescription')}
          />
          <Input
            type="number"
            name="taskImportanceNumber"
            label="Вaжность задачи"
            description="Введите важность задачи от 1 до 10"
            value={taskImportanceNumber}
            onChange={event => this.onChange(event, 'taskImportanceNumber')}
            require
          />
          <div className="d-flex justify-content-around">
            <Toggle
              valueFalse="Не важно"
              valueTrue="Важно"
              checked={taskImportance}
              onChange={this.changeHandlerImportance}
            />
            <Toggle
              valueFalse="Не срочно"
              valueTrue="Срочно"
              checked={taskUrgency}
              onChange={event => this.changeHandlerUrgency(event)}
            />
          </div>
        </div>
        <Button primary value="Добавить" onClick={this.clickHandlerSubmit} disabled={this.validate()} />
      </>
    );
  }
}

TodoInputs.propTypes = {
  submitTask: PropTypes.func.isRequired,
};

function mapDisputhToProps(dispath) {
  return {
    submitTask: (
      taskName,
      taskDescription,
      taskImportanceNumber,
      taskImportance,
      taskUrgency,
    ) => dispath(submitNewTask(
      taskName,
      taskDescription,
      taskImportanceNumber,
      taskImportance,
      taskUrgency,
    )),
  };
}

export default connect(null, mapDisputhToProps)(TodoInputs);
