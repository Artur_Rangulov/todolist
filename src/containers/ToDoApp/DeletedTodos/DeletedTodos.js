import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Card from '../../../components/Card/Card';
import styles from './deletedTodos.scss';


const DeletedTodos = (props) => {
  const { deletedTodos } = props;
  return (
    <>
      <h4 className="border-bottom pb-3">Удаленные задачи</h4>
      <div className={`mt-4 ${styles.completedtasks}`}>
        <Card tasks={deletedTodos} />
      </div>
    </>
  );
};

DeletedTodos.propTypes = {
  deletedTodos: PropTypes.instanceOf(Array),
};

DeletedTodos.defaultProps = {
  deletedTodos: [],
};


function mapStateToProps(state) {
  return {
    deletedTodos: state.todoapp.deletedTodos,
    tasks: state.todoapp.tasks,
  };
}

// function mapDisputhToProps(dispath){
//   return {
//   }
// }

export default connect(mapStateToProps)(DeletedTodos);
