import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Card from '../../../components/Card/Card';
import styles from './completedtasks.scss';


const CompletedTasks = (props) => {
  const { completedTasks } = props;
  return (
    <>
      <h4 className="border-bottom pb-3">Завершенные задачи</h4>
      <div className={`mt-4 ${styles.completedtasks}`}>
        <Card tasks={completedTasks} />
      </div>
    </>
  );
};

CompletedTasks.propTypes = {
  completedTasks: PropTypes.instanceOf(Array),
};

CompletedTasks.defaultProps = {
  completedTasks: [],
};

function mapStateToProps(state) {
  return {
    completedTasks: state.todoapp.completedTasks,
    todos: state.todoapp.todos,
  };
}

export default connect(mapStateToProps)(CompletedTasks);
