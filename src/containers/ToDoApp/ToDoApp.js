import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TodoInputs from './ToDoInput/TodoInputs';
import TodoList from './TodoList/TodoList';
import EisenhowerMatrix from './EisenhowerMatrix/EisenhowerMatrix';
import CompletedTasks from './CompletedTasks/CompletedTasks';
import DeletedTodos from './DeletedTodos/DeletedTodos';
import styles from './todoapp.scss';
import Loading from '../../components/Loading/Loading';
import CurrentDate from '../../components/CurrentDate/CurrentDate';
import { ClockGlyph } from '../../components/Glyph/Glyph';
import { getTodos, postTodos } from '../../store/actions/todoapp';

class ToDoApp extends Component {
  state = {
    loaded: false,
  }

  componentWillMount() {
    const { ...state } = this.state;
    const { getTodos1, getST } = this.props;
    getTodos1();
    this.setState({
      ...state,
      loaded: true,
    });
    console.log(getST());
  }

  render() {
    console.log(this.props);
    const { loaded } = this.state;
    return (
      <div className="container app mt-3">
        <div className="row justify-content-end">
          <div className={styles.currentdate}>
            <ClockGlyph color="#000" />
            <CurrentDate />
          </div>
        </div>
        {
          loaded
            ? (
              <div className="row">
                <div className="col"><TodoInputs /></div>
                <div className="col"><TodoList /></div>
                <div className="w-100" />
                <div className="col"><EisenhowerMatrix /></div>
                <div className="w-100" />
                <div className="col"><CompletedTasks /></div>
                <div className="w-100" />
                <div className="col"><DeletedTodos /></div>
              </div>
            )
            : <Loading />
        }
      </div>
    );
  }
}


// function mapDisputhToProps(dispath){
//   return {
//     submitNewTask: (
//       taskName,
//       taskDescription,
//       taskImportanceNumber,
//       taskImportance,
//       taskUrgency
//     ) => dispath(submitNewTask(
//       taskName,
//       taskDescription,
//       taskImportanceNumber,
//       taskImportance,
//       taskUrgency
//     )),
//     deleteTaskItem: id => dispath(deleteTaskItem(id)),
//     sendToListOfTasks: id => dispath(sendToListOfTasks(id)),
//     sendToMatrix: id => dispath(sendToMatrix(id)),
//     completeTask: id => dispath(completeTask(id))
//   }
// }

ToDoApp.propTypes = {
  getTodos1: PropTypes.func.isRequired,
  getST: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  ...state,
});

const mapDispatchToProps = dispatch => ({
  getTodos1: () => dispatch(getTodos()),
  getST: () => dispatch(postTodos()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToDoApp);
