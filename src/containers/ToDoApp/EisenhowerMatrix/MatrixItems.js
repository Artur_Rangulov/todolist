const MatrixItems = [
  {
    name: 'Срочно и Важно ',
    taskImportance: true,
    taskUrgency: true,
  },
  {
    name: 'Важно, но не срочно ',
    taskImportance: true,
    taskUrgency: false,
  },
  {
    name: 'Срочно, но не важно ',
    taskImportance: false,
    taskUrgency: true,
  },
  {
    name: 'Не срочно и не важно ',
    taskImportance: false,
    taskUrgency: false,
  },
];

export default MatrixItems;
