import React from 'react';
import PropTypes from 'prop-types';
import Badge from '../../../components/Badge/Badge';
import Card from '../../../components/Card/Card';

const MatrixItemTemplate = ({
  name,
  taskImportance,
  taskUrgency,
  tasks,
  deleteItem,
  sendToListOfTasks,
  cardtype,
}) => {
  const sortableTasks = tasks
    .filter(task => task.taskImportance === taskImportance && task.taskUrgency === taskUrgency);
  return (
    <>
      <h6 className="border-bottom pb-3">
        {name}
        <Badge value={sortableTasks.length} info />
      </h6>
      <Card
        tasks={sortableTasks}
        deleteItem={deleteItem}
        sendToListOfTasks={sendToListOfTasks}
        cardtype={cardtype}
      />
    </>
  );
};

MatrixItemTemplate.propTypes = {
  name: PropTypes.string.isRequired,
  taskImportance: PropTypes.bool.isRequired,
  taskUrgency: PropTypes.bool.isRequired,
  tasks: PropTypes.instanceOf(Array),
  deleteItem: PropTypes.func,
  sendToListOfTasks: PropTypes.func,
  cardtype: PropTypes.string,
};

MatrixItemTemplate.defaultProps = {
  tasks: [],
  cardtype: '',
  deleteItem: () => {},
  sendToListOfTasks: () => {},
};

export default MatrixItemTemplate;
