import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import MatrixItemTemplate from './MatrixItemTemplate';
import * as UI from './EisenhowerMatrixStyle';
import MatrixItems from './MatrixItems';
import { deleteTaskItem, sendToListOfTasks } from '../../../store/actions/todoapp';

class EisenhowerMatrix extends Component {
  comparetaskImportance = (A, B) => (B.taskImportanceNumber - A.taskImportanceNumber)

  render() {
    const {
      tasks,
      deleteItem,
      sendToList,
    } = this.props;
    tasks.sort(this.comparetaskImportance);
    return (
      <div className="mt-5">
        <h4 className="border-bottom pb-3">Матрица Эйзенхауэра</h4>
        <UI.MatrixContainer>
          {MatrixItems.map(item => (
            <UI.Matrix key={item.name}>
              <MatrixItemTemplate
                name={item.name}
                taskImportance={item.taskImportance}
                taskUrgency={item.taskUrgency}
                tasks={tasks}
                deleteItem={deleteItem}
                sendToListOfTasks={sendToList}
                cardtype="taskslist"
              />
            </UI.Matrix>
          ))}
        </UI.MatrixContainer>
      </div>
    );
  }
}

EisenhowerMatrix.propTypes = {
  tasks: PropTypes.instanceOf(Array),
  deleteItem: PropTypes.func,
  sendToList: PropTypes.func,
};

EisenhowerMatrix.defaultProps = {
  tasks: [],
  deleteItem: () => {},
  sendToList: () => {},
};

function mapStateToProps(state) {
  return {
    todos: state.todoapp.todos,
    tasks: state.todoapp.tasks,
  };
}

function mapDisputhToProps(dispath) {
  return {
    deleteItem: id => dispath(deleteTaskItem(id)),
    sendToList: id => dispath(sendToListOfTasks(id)),
  };
}

export default connect(mapStateToProps, mapDisputhToProps)(EisenhowerMatrix);
