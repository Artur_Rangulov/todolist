import styled from 'styled-components';

export const MatrixContainer = styled.div`
  display:flex;
  flex-wrap: wrap;
`;

export const Matrix = styled.div`
  flex:1 1 calc(50% - 30px);
  margin:5px;
  box-sizing:border-box;
  padding:10px;
`;
