import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Card from '../../../components/Card/Card';
import Badge from '../../../components/Badge/Badge';
import styles from './todolist.scss';
import { sendToMatrix, completeTask } from '../../../store/actions/todoapp';


class TodoList extends Component {
  comparetaskImportance = (A, B) => B.taskImportanceNumber - A.taskImportanceNumber;

  render() {
    const {
      todos,
      send,
      complete,
    } = this.props;

    const isArray = Array.isArray(todos);
    if (isArray) {
      todos.sort(this.comparetaskImportance);
    }
    return (
      <>
        <h4 className="border-bottom pb-3">
          Список дел на сегодня
          <Badge value={isArray ? todos.length : 0} info />
        </h4>
        <div className={`${styles.todolist} mt-4`}>
          <Card tasks={todos} sendToMatrix={send} completeTask={complete} cardtype="todolist" />
        </div>
      </>
    );
  }
}

TodoList.propTypes = {
  todos: PropTypes.instanceOf(Array),
  send: PropTypes.func.isRequired,
  complete: PropTypes.func.isRequired,
};

TodoList.defaultProps = {
  todos: [],
};

function mapStateToProps(state) {
  return {
    todos: state.todoapp.todos,
    tasks: state.todoapp.tasks,
  };
}

function mapDisputhToProps(dispath) {
  return {
    send: id => dispath(sendToMatrix(id)),
    complete: id => dispath(completeTask(id)),
  };
}

export default connect(mapStateToProps, mapDisputhToProps)(TodoList);
