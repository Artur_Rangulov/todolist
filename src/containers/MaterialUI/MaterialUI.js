import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import { getNames, postNames, addName } from '../../store/actions/names';
import Input from '../../components/Input/Input';
import Button from '../../components/Button/Button';


class MaterialUI extends Component {
  state = {
    inputValue: '1',
  }

  componentWillMount() {
    const { getNames1 } = this.props;
    getNames1();
  }

  addName(inputValue) {
    const { addName1 } = this.props;
    addName1(inputValue);
    this.setState({
      inputValue: '',
    });
  }

  render() {
    const { names, postNames1 } = this.props;
    const { inputValue } = this.state;
    return (
      <div>
        <h1>Names</h1>
        <Input placeholder="Введите имя" onChange={event => this.setState({ inputValue: event.target.value })} value={inputValue} />
        <Button value="добавить" onClick={() => this.addName(inputValue)} disabled={inputValue.length === 0} />
        <ul>
          {
            Array.isArray(names) && names.map(item => (
              <li key={uuid()}>{item.name}</li>
            ))
          }
        </ul>
        <Button value="сохранить" onClick={() => postNames1()} />
      </div>
    );
  }
}

MaterialUI.propTypes = {
  getNames1: PropTypes.func,
  postNames1: PropTypes.func,
  addName1: PropTypes.func,
  names: PropTypes.instanceOf(Object),
};

MaterialUI.defaultProps = {
  getNames1: () => {},
  addName1: () => {},
  postNames1: () => {},
  names: [],
};

const mapStateToProps = state => ({
  ...state,
  names: state.names.names,
});

const mapDispatchToProps = dispatch => ({
  getNames1: () => dispatch(getNames()),
  postNames1: () => dispatch(postNames()),
  addName1: name => dispatch(addName(name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MaterialUI);
