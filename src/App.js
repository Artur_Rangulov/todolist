import React from 'react';
import {
  BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';
import ToDoApp from './containers/ToDoApp/ToDoApp';
import Error404 from './components/Error404/Error404';
import Menu from './components/Menu/Menu';
import Random from './testpages/Random/Random';
// import Hooks from './testpages/Hooks/Hooks';
import Dashboard from './containers/Dashboard/Dashboard';
import Home from './testpages/Home/Home';
import {
  HomeGlyph,
  ToDoListGlyph,
  RandomGlyph,
  DefaultGlyph,
  DashbooardGlyph,
} from './components/Glyph/Glyph';
import styles from './appStyle.scss';
import MaterialUI from './containers/MaterialUI/MaterialUI';

const links = [
  { link: '/', title: 'Home', icon: <HomeGlyph /> },
  { link: '/app/', title: 'App', icon: <ToDoListGlyph /> },
  { link: '/dashboard/', title: 'Dashboard', icon: <DashbooardGlyph /> },
  { link: '/random/', title: 'Random', icon: <RandomGlyph /> },
  { link: '/hooks/', title: 'Hooks', icon: <DefaultGlyph /> },
  { link: '/materialui/', title: 'MaterialUI', icon: <DefaultGlyph /> },
];

function AppRouter() {
  return (
    <Router>
      <div className={`${styles.container}`}>
        <Menu links={links} />
        <div className={`${styles.content}`}>
          <Switch>
            <Route path="/random/" component={Random} />
            <Route path="/app/" component={ToDoApp} />
            {/* <Route path="/hooks/" component={Hooks} /> */}
            <Route path="/dashboard/" component={Dashboard} />
            <Route path="/materialui/" component={MaterialUI} />
            <Route path="/" exact component={Home} />
            <Route component={Error404} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default AppRouter;
