import React, { useState, useEffect } from 'react'

const Hooks = () => {
  const [count, setcount] = useState(0)
  const [countClick, setcountClick] = useState(0)
  const [statusIsOnline, setStatusIsOnline] = useState(false)

  useEffect(() => {
    document.title = `Status: ${statusIsOnline ? 'Online' : 'Offline'}`
    return () => {}
  })

  return (
    <div className='mt-3'>
      <h2>Counter: {count} </h2>
      <h2>Count of buttons click: {countClick} </h2>
      <div>
        <button
          type="button"
          className='btn btn-outline-primary'
          onClick={() => { setcount(count - 1); setcountClick(countClick + 1) }}
        > -1 </button>
        <button
          type="button"
          className='btn btn-outline-primary'
          onClick={() => { setcount(count + 1); setcountClick(countClick + 1) }}
        > +1 </button>
        <button
          type="button"
          className='btn btn-outline-primary'
          onClick={() => { setcount(0); setcountClick(countClick + 1) }}
        > Обнулить</button>
      </div>
      <hr />
      <div className='mt-3'>
        <button
          className={'btn btn-primary'}
          onClick={() => { setStatusIsOnline(!statusIsOnline); setcountClick(countClick + 1) }}
        >
          To set status {statusIsOnline ? 'Offline' : 'Online'}
        </button>
        <p className="mt-3">P.S. Look at the document title</p>
      </div>
    </div>
  )
}

export default Hooks
