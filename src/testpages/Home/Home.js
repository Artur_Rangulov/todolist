import React from 'react';
import { Link } from 'react-router-dom';

const Home = () => (
  <div>
    <p>Hello! This is my first app with React.js </p>
    <p>Use stack</p>
    <div>
      <span>Css</span>
      <ul>
        <li>css-modules</li>
        <li>bootstrap</li>
        <li>styled-component</li>
      </ul>
    </div>
    <Link to="/app/">Discover App</Link>
  </div>
);


export default Home;
