import React, { Component } from 'react';
import Button from '../../components/Button/Button';
import Loading from '../../components/Loading/Loading';

export default class Random extends Component {
  state = {
    message: '',
    loading: true,
  }

  getLucky = () => {
    let message;
    let messageColor;

    this.setState(previousState => ({
      ...previousState,
      message: '',
      loading: false,
    }));

    if (Math.floor(Math.random() * 10) >= 5) {
      message = 'You are lucky & saved the world :)';
      messageColor = 'text-success';
    } else {
      message = 'You are not lucky & world has destroyed :(';
      messageColor = 'text-danger';
    }

    setTimeout(() => {
      this.setState(previousState => ({
        ...previousState,
        message,
        messageColor,
        loading: true,
      }));
    }, 1000);
  }

  render() {
    const { message, messageColor, loading } = this.state;
    return (
      <div className="d-flex flex-column align-items-center mt-3">
        <h2>Hello, my friend! </h2>
        <p>Push the button, please</p>
        <div><Button onClick={this.getLucky} value={message ? 'Try again' : 'Get lucky'} primary /></div>
        {loading
          ? <h1 className={`${messageColor} mt-3`}>{message}</h1>
          : <Loading className="mt-3" />
        }
      </div>
    );
  }
}
