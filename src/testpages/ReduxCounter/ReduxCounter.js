import React from 'react'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import RootReducer from './RootReducer/RootReducer'
import Counter from './Counter'


const store = createStore(RootReducer)

const ReduxCounter = (
    <Provider store={store}>
        <Counter />
    </Provider>
)

export default ReduxCounter
